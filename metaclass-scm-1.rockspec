rockspec_format = "1.1"
package = "metaclass"
version = "scm-1"
source = {
   url = "git+https://gitlab.com/ochaton/metaclass"
}
description = {
   summary = "OOP Framework for Lua",
   homepage = "https://gitlab.com/ochaton/metaclass",
   license = "Artistic2"
}
dependencies = {
   "lua ~> 5.1"
}
build = {
   type = "builtin",
   modules = {
      ["metaclass.containers.Enum"] = "src/metaclass/containers/Enum.lua",
      ["metaclass.containers.List"] = "src/metaclass/containers/List.lua",
      ["metaclass.containers.init"] = "src/metaclass/containers/init.lua",
      metaclass = "src/metaclass.lua"
   }
}
